package com.spring.shop.vo;

import lombok.Data;

@Data
public class ProductVO {
	private int proNum;
	private String proCategory;
	private String proTitle;
	private String proDate;
	private int proPrice;
	private String proContent;
	private String proImage;
	private String proStatus;
	private String proClickCount;
}
