package com.spring.shop.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.shop.vo.ProductVO;
import com.spring.shop.dao.ProductDAO;


@Service
public class ProductService {
	
	@Autowired
	private ProductDAO dao;

	public ArrayList<ProductVO> getList() {
		return dao.getList();
	}

	public ArrayList<ProductVO> getPantsList() {
		// TODO Auto-generated method stub
		return dao.getPantsList();
	}

	public ArrayList<ProductVO> getSkirtList() {
		// TODO Auto-generated method stub
		return dao.getSkirtList();
	}

	public ArrayList<ProductVO> getOutwearList() {
		// TODO Auto-generated method stub
		return dao.getOutwearList();
	}

	public ArrayList<ProductVO> getShoesList() {
		// TODO Auto-generated method stub
		return dao.getShoesList();
	}

	public ArrayList<ProductVO> getAccessoriesList() {
		// TODO Auto-generated method stub
		return dao.getAccessoriesList();
	}

}
