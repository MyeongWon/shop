package com.spring.shop.dao;

import java.util.ArrayList;

import com.spring.shop.vo.ProductVO;

public interface ProductMapper {

	ArrayList<ProductVO> getList();

	ArrayList<ProductVO> getPantsList();

	ArrayList<ProductVO> getSkirtList();

	ArrayList<ProductVO> getOutwearList();

	ArrayList<ProductVO> getShoesList();

	ArrayList<ProductVO> getAccessoriesList();

}
