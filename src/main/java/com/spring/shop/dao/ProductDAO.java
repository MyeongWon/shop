package com.spring.shop.dao;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.shop.vo.ProductVO;

@Repository
public class ProductDAO {
	
	@Autowired
	private SqlSession session;

	public ArrayList<ProductVO> getList() {
		ArrayList<ProductVO> result = null;
		System.out.println(result);
		
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			result = mapper.getList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		System.out.println(result);
		return result;
	}

	public ArrayList<ProductVO> getPantsList() {
		ArrayList<ProductVO> result = null;
		System.out.println(result);
		
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			result = mapper.getPantsList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		System.out.println(result);
		return result;
	}

	public ArrayList<ProductVO> getSkirtList() {
		ArrayList<ProductVO> result = null;
		System.out.println(result);
		
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			result = mapper.getSkirtList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		System.out.println(result);
		return result;
	}

	public ArrayList<ProductVO> getOutwearList() {
		ArrayList<ProductVO> result = null;
		System.out.println(result);
		
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			result = mapper.getOutwearList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		System.out.println(result);
		return result;
	}

	public ArrayList<ProductVO> getShoesList() {
		ArrayList<ProductVO> result = null;
		System.out.println(result);
		
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			result = mapper.getShoesList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		System.out.println(result);
		return result;
	}

	public ArrayList<ProductVO> getAccessoriesList() {
		ArrayList<ProductVO> result = null;
		System.out.println(result);
		
		try {
			ProductMapper mapper = session.getMapper(ProductMapper.class);
			result = mapper.getAccessoriesList();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		System.out.println(result);
		return result;
	}

}
