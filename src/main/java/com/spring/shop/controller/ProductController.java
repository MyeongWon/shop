package com.spring.shop.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.spring.shop.service.ProductService;
import com.spring.shop.vo.ProductVO;

@Controller
@RequestMapping(value = "/product")
public class ProductController {
	
	@Autowired
	private ProductService service;
	
	@RequestMapping(value = "/productList", method = RequestMethod.GET)
	public String productList(Model model) {
		
		ArrayList<ProductVO> productList = service.getList();
		if(productList.size() == 0) {
			System.out.println("호출 실패");
			return "home";
		}
		else {
			model.addAttribute("list", productList);
			return "product/productList";
		}
			
		
		
	}
	
	@RequestMapping(value = "/pantsList", method = RequestMethod.GET)
	public String pantsList(Model model) {
		
		ArrayList<ProductVO> productList = service.getPantsList();
		if(productList.size() == 0) {
			System.out.println("호출 실패");
			return "home";
		}
		else {
			model.addAttribute("list", productList);
			return "product/productList";
		}		
		
	}
	
	@RequestMapping(value = "/skirtList", method = RequestMethod.GET)
	public String skirtList(Model model) {
		
		ArrayList<ProductVO> productList = service.getSkirtList();
		if(productList.size() == 0) {
			System.out.println("호출 실패");
			return "home";
		}
		else {
			model.addAttribute("list", productList);
			return "product/productList";
		}
			
	}
	
	@RequestMapping(value = "/outwearList", method = RequestMethod.GET)
	public String outwearList(Model model) {
		
		ArrayList<ProductVO> productList = service.getOutwearList();
		if(productList.size() == 0) {
			System.out.println("호출 실패");
			return "home";
		}
		else {
			model.addAttribute("list", productList);
			return "product/productList";
		}
	}
	
	@RequestMapping(value = "/shoesList", method = RequestMethod.GET)
	public String shoesList(Model model) {
		
		ArrayList<ProductVO> productList = service.getShoesList();
		if(productList.size() == 0) {
			System.out.println("호출 실패");
			return "home";
		}
		else {
			model.addAttribute("list", productList);
			return "product/productList";
		}
	}
	
	@RequestMapping(value = "/accessoriesList", method = RequestMethod.GET)
	public String accessoriesList(Model model) {
		
		ArrayList<ProductVO> productList = service.getAccessoriesList();
		if(productList.size() == 0) {
			System.out.println("호출 실패");
			return "home";
		}
		else {
			model.addAttribute("list", productList);
			return "product/productList";
		}
	}
	
	
}
