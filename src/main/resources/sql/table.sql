create table shop_user (
    user_num number not null,
    user_id varchar2(20) not null,
    user_password varchar2(24) not null,
    user_address varchar2(120) not null,
    user_phone varchar2(11) not null,
    user_status number(1) default 1,
    user_email varchar2(20) not null
);

alter table shop_user add primary key(user_num);
alter table shop_user add unique(user_id);
alter table shop_user add check(user_status BETWEEN 1 and 2);

create table shop_product (
    pro_num number not null,
    pro_category varchar2(30) not null,
    pro_title varchar2(120) not null,
    pro_date date default sysdate,
    pro_price number not null,
    pro_content varchar2(4000) not null,
    pro_image varchar2(2000),
    pro_status number default 1,
    pro_clickcount number default 0
);

alter table shop_product add primary key(pro_num);
alter table shop_product add check(pro_status between 1 and 2);

create table shop_board (
    board_num number not null,
    board_witer varchar2(20) not null,
    board_title varchar2(200) not null,
    board_content varchar2(4000) not null,
    board_date date default sysdate
);

create sequence usersq nocache;
create sequence prosq nocache;
create sequence boardsq nocache;